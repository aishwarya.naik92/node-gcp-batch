
// You can enforce that only string go in this array
const names: string[] = [] 
//const names: Array<string> = []
names.push("Adam");
names.push('Rick');
// names.push(90);

const numsOrStrings:Array<string | number> = []
numsOrStrings.push("Hello");
numsOrStrings.push(9);
// numsOrStrings.push(true); 
