// TypeScript has in-built support for OOP
// TS had classes BEFORE JS
// TS has access modifiers
// TS has interface

class Person{

    name:string;
    age:number;
    private ssid:number//  access modifier. Cannot be accessed outside of the class

    constructor(name:string, age:number, ssid:number){
        this.name = name;
        this.age = age;
        this.ssid = ssid;

    }

}

const adam:Person = new Person("Adam",19,5555555555);
console.log(adam)
adam.age
adam.name
//adam.ssid // error for accessing private variable

class Trainer extends Person{

    sayHello(){
        console.log(`Hello my name is ${this.name}`)
    }
}