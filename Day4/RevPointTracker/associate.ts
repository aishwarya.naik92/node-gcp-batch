// export will allow us to use this class in another file
// default means we do not need curly bracket when we import it
export default class Associate{
    constructor(public name:string, public points:number){}
}