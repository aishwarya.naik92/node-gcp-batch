// JS has 6 primitive types
// primitive is a semantic term.
// Some of the primitives ARE objects 

const fname = "Adam"; // string
console.log(typeof fname);// typeof operator used for getting the type

const num = 90; // number
const num2 = 9.1; // number
// there are no shorts floats or ints in JS

const isAvailable = true; // boolean

const nothing = null // null

const undef = undefined // undefined

// The last primitive is Symbol
// Added in ES6 most don't have a reason to use it

// null vs undefined
// null is a value that must EXPLICTLY assigned to a variable or returned from a function
// The programmer assign null as a conscience decision
// undefined is just the default value of everything in JS

let n; // n NEVER had a value
console.log(n); // undefined

let y = null; // Explicitly assign y the value of null
console.log(y); // null

// from a debugging perspecitve I like this
// if you get an exepected undefined that means something NEVER had value 
// if you get null then somewhere in the code it was given the value null
