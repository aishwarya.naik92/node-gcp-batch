// == compares varaibles based 100% on the value
// === (strict equality) compares variables based on TYPE and value

console.log("100" == 100);// true
console.log("100" === 100);// false
// Moral of the story ALWAYS use === unless you have a specific really good reason to use ==