
function letVsConst(){
    let x = 100;
    x = 50
    x = "Hello"
    console.log(x)
}

letVsConst()
//console.log(x)// error

// default your mind into declaring variables with const and reserve let for when you actually
// reassign a value to that variable