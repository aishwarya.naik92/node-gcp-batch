// JS has three scopes
// Global scope (no keyword)
// Function scope var
// Block scope let const
// Scopes in JS are defined via key words 

y = 1000 // this is now a global variables that can be used ANYWHERE in the program
// Avoid global variables when you can
// they can make code REALLY hard to follow and debug

console.log(y)

function printY(){
    console.log("Print y from a function " + y) // function can use the  global variable no problem
}

function alterY(){
    y = --y;// decrease the value of y by one
}

printY()
alterY()
printY()
alterY()
printY()

function createGlobal(){
    x = 100; // you can create global variables from within a function
    // again this can make your code difficult to debug
}

createGlobal()

console.log(x)