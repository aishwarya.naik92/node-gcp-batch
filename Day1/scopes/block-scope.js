// ES6 2015 introduced introduced const and let
// The are keywords for making block scope variables
// they cannot be hoisted** Will get an error if you try

function holaMundo(){
    const greeting = "Hola Todos";
    console.log(greeting)
}
// console.log(greeting)// error
holaMundo()

function blockScoping(num){

    if(num < 50){
        const h = 100;
        console.log(h)
    }
    console.log(h) // ERROR the variable h at line 15 cannot be used outside of
    // the brackets it was declared in
}

//blockScoping(30)

function noHoist(){
    console.log(person);
    const person = "Adam Ranieri";
}

// noHoist()