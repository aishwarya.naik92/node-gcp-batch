import React from 'react';
import ReactDOM from 'react-dom';
import ContactInfo from './components/contact-info';
import Counter from './components/counter';
import CurriculaList from './components/curricula-list';
import EmployeeList from './components/employee-list';
import GreetButton from './components/greet-btn';
import MoreInfoForm from './components/more-info-form';
import ToDoList from './components/to-do-list';
import WelcomeMessage from './components/welcome-message'

// this ReactDOM.render function is what actually renders and displays your web page.
// if you code is not being called here it is NOT being used
ReactDOM.render(
  <React.StrictMode>
    {/* <WelcomeMessage></WelcomeMessage> 
    <ContactInfo></ContactInfo>
    <CurriculaList></CurriculaList>
    <EmployeeList></EmployeeList>
    <GreetButton></GreetButton>
    <MoreInfoForm></MoreInfoForm> */}
    <Counter></Counter>
    <ToDoList></ToDoList>
  </React.StrictMode>,
  document.getElementById('root')
);
