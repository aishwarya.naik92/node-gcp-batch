import EmployeeItem from "./employee-item"

export default function EmployeeList(){

    const hrEmployees = [
        {name:"Shelby Lloyd",title:"Associate Mentor"},
        {name:"Cameron Coley", title:"HR Lead"},
        {name:"Carol Baxtoer", title:"Head of HR"}
    ]

    const hrEmployeeList = hrEmployees.map(e=><EmployeeItem name={e.name} title={e.title}></EmployeeItem>);

    return(<div>
        <h3>Employee Information</h3>
        <h4>Tech Team</h4>
        <ul>
            <EmployeeItem name="Adam Ranieri" title="Lead Trainers"></EmployeeItem>
            <EmployeeItem name="Richard Orr" title="Tech Manager"></EmployeeItem>
            <EmployeeItem name="Sierra Nicholes" title="Senior Trainer"></EmployeeItem>
            {/* The attributes on a component are used to pass in our input props to the function */}
        </ul>
        <h4>HR team</h4>
        <ul>
            {hrEmployeeList}
        </ul>
    </div>)
}