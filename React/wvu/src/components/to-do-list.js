import { useRef, useState } from "react";


export default function ToDoList(){

    const [todos,setTodos] = useState(["do the dishes","do the laundry"]);
    const todoList = todos.map(t=><li>{t}</li>);

    const todoInput = useRef(null);

    function addToDo(event){
        const value = todoInput.current.value;
        const newArray = [...todos, value];// fastest way to create a brand new array that
        // has all the elements of todos along with a new value
        setTodos(newArray)
        todoInput.current.value = ''
    }


    return(<div>
        <ol>
            {todoList}
        </ol>

        <input placeholder="put todo here" ref={todoInput}></input>
        <button onClick={addToDo}>add to do to list</button>

    </div>)

}