
export default function ContactInfo(){
// the react philosophy is that html/css/js SHOULD blur together becuase they are so tied to each other for making a web page
// you can insert JS ANYWHERE in your JSX  {}
// interpolating the value of phoneNumber
    const phoneNumber = '304-555-5555'


    return(<div>
        <h3>Phone number</h3>
        <p>{phoneNumber}</p>

        <h3>Street Address </h3>
        <p> 496 High St Morgantown WV 26505</p>
    </div>)

}