import {useState } from "react"


export default function Counter(){

    // react hook to create STATEFUL variables that can be re-rendered in our JSX
    // [statefulvalue (read-only), functionthatREPLACESthestatefulvalue]
    // REPLACES
    const [num,setNum] = useState(0); // default value

    function addToNum(){
        const newValue = num + 1;
        setNum(newValue)// replacing the old value with a new value
        // think of setNum as telling react to RE-RENDER the component using the new value
    }



    // a component is just a function. it returns a certain JSX.
    // React needs to know when a component should be "re-rendered"
    // because some value inside of it is being altered THAT is to display in the JSX
    return(<div>
        <h3>Counter Value {num} </h3>
        <button onClick={addToNum}>Add one to num</button>
    </div>)

}