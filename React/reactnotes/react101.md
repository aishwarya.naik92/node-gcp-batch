# React
- JavaScript Libary for creating webpages/ websites
- The most popular library/framework for creating websites
    - Angular
    - vue
    - Svelte
- Created by Facebook
- **Single page Applications** (SPA)
    - Regardless of how many pages you have making up a website the actual page never changes.
        - HTML is dynamically altered, removed and added to give the illusion of many pages
- **Component Based**
    - A component is going to be a reusable chunk of html/css/js
    - Component based design will allow us to make web pages in small pieces.
        - Scalable
        - Modular (easy to swap in and out parts)
- Statefulness
    - React gets it's name from being reactive.
    - The UI can react or change to certain events
    - Some components are stateful which means their is some value that should be stored specific for that component.