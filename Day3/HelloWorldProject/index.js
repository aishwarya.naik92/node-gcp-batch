//const calcFunctions = require("./calculator-functions");
// using require and module.exports is called commonJS syntax
import {add, multiply} from './calculator-functions.js';
import sayHello from './greet-functions.js' 
import Pie from 'cli-pie';

console.log("Hello from the index.js")
console.log(add(100,50))
console.log(multiply(100,50))
sayHello()

const p = new Pie(5, [
    { label: "Water", value: 50, color: [ 0, 0, 255] }
  , { label: "Land", value: 50, color: [255, 240, 0] }
], {
    legend: true
});

console.log(p.toString());