## Node Project 101
- Every node project has a ***package.json***
- This is a configuration file for your project
    - Description of the project
    - List the authors
    - List of any dependenies for the project
    - It can contain scripts/ commands that you want people to use on your poject
    - a "main" which is the central starting point file of your application
## NPM
- Node Package Manager
- Software tool for installing and managing dependencies
- A **dependency** is any 3rd code that we have added to our project
- NPM repository is a code repo for JS
    - We can install anything on NPM 
    - "Install" download the JS files
- Any depedencies you install are stored in the node_modules folder
    - This folder gets MASSIVE in a lot of projects
