
// if a file is exporting a single value you can put default after the export
// the ONLY thing this does it make it so you do not need curly brackets to import
export default function sayHello(){
    console.log("Hello")
}