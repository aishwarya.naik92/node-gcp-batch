// JS being dynamicly typed it can be hard to know if an object has a property or not
// Use the && and || operators to act as guards and default values

const ricky = {fname:"Richard", lname:"Orr", mi:"L"};
const steve = {fname:"Steven", lname:"Kelsey"}

function printFullName(person){
    const fname = person.fname || "No First Name" ;
    const lname = person.lname || "No Last Name";
    const mi = person.mi || "No Middle Initial";
    console.log(`Hello ${fname} ${mi} ${lname}`);
}

printFullName(steve)
// || and && are actually selectors they return the truthy value
const result = "Hello" || false;// Does not return a boolean
console.log(result)
