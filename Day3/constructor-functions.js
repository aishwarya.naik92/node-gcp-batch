// Constructor functions allow us to consistently makes objects with defined properties
// Constructor functions are Capitalized by convention


function Person(fname, lname, age){
    console.log("Creating a person")
    this.fname = fname;
    this.lname = lname;
    this.age = age;
    // anonymous function because it was defined on the spot not previously defined
    this.describeSelf = function(){
        console.log("Hello my name is " + this.fname)
    }
}

// new keyword is necessary
const adam = new Person("Adam", "Ranieri", 19);
console.log(adam)
adam.describeSelf()