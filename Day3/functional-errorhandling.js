function validateUsername(username = "", err){
    
    if(username.length<6 ){
        const usernameTooShortError = {
            message:"Username was too short",
            cause:"Username must be of length 6 but was " + username.length};
        err(usernameTooShortError)
    }else if(username.includes("?") || username.includes("!")){
        const invalidCharacterError = {message:"Username contains invalid character"};
        err(invalidCharacterError)
    }else{
        return true;
    }
};