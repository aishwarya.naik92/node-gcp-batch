// functions ARE objects
// everytime you create a function you are creating a brand new object in memory

function sayHello(){
    const adam = {fname:"Adam"};
    
    const greet = function(){ // The variable at line 5 is enclosed within the function
        // closure
        console.log("Hello" + adam.fname)
    }

    return greet;

}
const hello2 = sayHello();// Returns a function
console.log(hello2)
hello2()

// Everytime you call the function, all variables and functions within it are created as new instances
function createCounter(){

    let x = 0;

    const addOne = function(){
        x++;
        console.log(x)
    }

    return addOne;
}

// They return DIFFERENT function objects
let counter1 = createCounter() // return a function
let counter2 = createCounter() // returns a function
counter1()
counter1()
counter1()
counter1()

counter2()
counter2()
counter2()
counter2()

function customizedGreeter(paramname){
    const name = paramname;

    const greet = function(){
        console.log("Hello " + name)
    }

    return greet;
}
const adamGreeter = customizedGreeter("Adam");
const alejandraGreeter = customizedGreeter("Alejandra");
adamGreeter()
alejandraGreeter()