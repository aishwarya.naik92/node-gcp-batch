// Errors are just objects that hold information on what went wrong
// 1. Where the problem occured
// 2. What caused it
// (3). Sometimes they tell you how to fix the problem

// Return true if that is a valid username or false if it is not
// usernames must be at least 6 characters in length
// usernames cannot use the charcters !, ?
function validateUsername(username = ""){
    
    if(username.length<6 ){
        const usernameTooShortError = {
            message:"Username was too short",
            cause:"Username must be of length 6 but was " + username.length};
        throw usernameTooShortError; // immediately end execution and generate an error in the code
    }else if(username.includes("?") || username.includes("!")){
        const invalidCharacterError = {message:"Username contains invalid character"};
        throw invalidCharacterError;
    }else{
        return true;
    }
};
// without errors you get return values that say or imply it did not work
// but it does not say why it did not work or where it happend
// A lot of times  you have code that might throw an error
// If an error is thrown and is not caught at some point your program might crash "generally not good"
// If you are calling functions that might throw an error you usually wrap them in a try catch block
// in the try block you put the code you want to run
// in the catch you put the code to run if you were to catch an error
try {
    validateUsername("billybob")
    console.log('Your username is all good')
} catch (error) { // the error object that was thrown is the error on this line
    if(error.message === "Username was too short"){
        console.log("You need a longer username try a compound word");
    }
    if(error.message === "Username contains invalid character"){
        console.log("Cannot use ? or !. Try other characters like & < > { }");
    }
} finally{
    console.log("Code in a finally block always executes")
}

