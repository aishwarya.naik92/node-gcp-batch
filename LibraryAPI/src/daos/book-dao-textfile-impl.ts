import { Book } from "../entities";
import { BookDAO } from "./book-dao";
import {readFile, writeFile} from 'fs/promises';
import { MissingResourceError } from "../errors";


export class BookDaoTextFile implements BookDAO{

    async createBook(book: Book): Promise<Book> {
        const fileData:Buffer = await readFile('C:\\Users\\AdamRanieri\\Desktop\\LibraryAPI\\books.txt')
        const textData:string = fileData.toString(); // turn file into character data
        const books:Book[] = JSON.parse(textData); // take the JSON text and turn it into an object
        book.bookId = Math.round(Math.random()*1000);// random number id for the book
        books.push(book);// add our book to the array
        await writeFile('C:\\Users\\AdamRanieri\\Desktop\\LibraryAPI\\books.txt',JSON.stringify(books));
        return book;
    }
    
    async getAllBooks(): Promise<Book[]> {
        const fileData:Buffer = await readFile('C:\\Users\\AdamRanieri\\Desktop\\LibraryAPI\\books.txt')
        const textData:string = fileData.toString(); 
        const books:Book[] = JSON.parse(textData);
        return books;
    }

    async getBookById(bookId: number): Promise<Book> {
        const fileData:Buffer = await readFile('C:\\Users\\AdamRanieri\\Desktop\\LibraryAPI\\books.txt')
        const textData:string = fileData.toString(); 
        const books:Book[] = JSON.parse(textData);

        for(const book of books){
            if(book.bookId === bookId){
                return book;
            }
        }
        throw new MissingResourceError(`The book with id ${bookId} could not be located`);
    }
    async updateBook(book: Book): Promise<Book> {
        const fileData:Buffer = await readFile('C:\\Users\\AdamRanieri\\Desktop\\LibraryAPI\\books.txt')
        const textData:string = fileData.toString(); 
        const books:Book[] = JSON.parse(textData); 

        for(let i =0; i< books.length; i++){
            if(books[i].bookId === book.bookId){
                books[i] = book;
            }
        }
        
        await writeFile('C:\\Users\\AdamRanieri\\Desktop\\LibraryAPI\\books.txt',JSON.stringify(books));
        return book;
    }

    // async functions MUST ALWAYS return a promise
    async deleteBookById(bookId: number): Promise<boolean> {
        const fileData:Buffer = await readFile('C:\\Users\\AdamRanieri\\Desktop\\LibraryAPI\\books.txt')
        const textData:string = fileData.toString(); 
        const books:Book[] = JSON.parse(textData); 

        for(let i =0; i< books.length; i++){
            if(books[i].bookId === bookId){
                books.splice(i)// remove that book
                await writeFile('C:\\Users\\AdamRanieri\\Desktop\\LibraryAPI\\books.txt',JSON.stringify(books));
                return true;
            }
        }
        
        return false;
    }

    
}