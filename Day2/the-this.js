// the this keyword is very tricky
// this in JS is dynamcially bound
// what 'this' depends on HOW the function was call NOT where it was created

x = 1000;

function printX(){
    console.log(this.x) 
}

printX() // if you call a function by itself the this will refer to global namespce

let obj = {
    x:"Hello",
    sayHello: printX
}
obj.sayHello()// prints hello beacuse the function is called as a method not a standalone function
