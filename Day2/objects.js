// JS originally had no classes. There was originall no template to create objects
// In JS objects are essentially a collection of key value pairs
// each key value pair is called a property
// Objects created without using a class are called object literals
// values can be ANYTHING

// const does not mean it cannot be altered it means it cannot be reassinged
// properties are seperated via a comma
const adam = {
    fname:"Adam", 
    lname:"Ranieri",
    age:19
};

function greetPerson(person){
    console.log("Hello " + person.fname);
}

//Objects in JS are always mutable
// you can add, remove or edit properites at any time
adam.location = "West Viginina"; // adds a new property to the Object
console.log(adam)

adam.fname = "Bob";
console.log(adam)

// you can remove a property using the delete keyword
delete adam.lname;
console.log(adam)

const bill = {
    fname:"Bill",
    lname:"Smith",
    // functions that are a property on an object are called methods
    // methods use the 'this'keyword to refer to properites on the object
    introduceSelf: function(){
        console.log("Hello my name is " + this.fname)
    }

}
const x = null; // null
const y = {};// empty object

bill.introduceSelf()
// function vs method
// function are standalone
// methods are functions attached to an object

// enhanced object literal
const enhancedBill ={
    fname:"Super Bill",
    lname:"James",
    // newer syntax for declaring methods on an object
    // identical to line 36
    introduceSelf(){
        console.log("Hello my name is " + this.fname)
    }
};

// There are 2 ways to access properties on an object
// 1. is dot notation
enhancedBill.fname // gets fname
//2. bracket notation
enhancedBill["fname"] // gets fname

function hasProperty(obj, prop){

    const result = obj[prop]
    if(result){
        console.log("Yes it has the property " + prop)
    }else{
        console.log("Nope")
    }
}

