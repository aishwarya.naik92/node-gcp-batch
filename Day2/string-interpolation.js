// Formatting strings in JS

const fname = "Adam";
const lname = "Ranieri";
//const fullName = "My Full name is " + fname + " " + lname;
const fullName = `My Full name is ${fname} ${lname}`;
console.log(fullName);

const operation = `The sum of the two numbers is ${7*9}`;
console.log(operation);

const nums = [10,30,50];
console.log(`hello ${nums[1]}`)