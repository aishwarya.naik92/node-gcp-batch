// JS has arrays which are objects that store values via a numbered index
// JS arrays can hold anything
// they dynamically resize as you add or remove elements

const stuff = ["Adam", "Bill", 9, null, {fname:"Adam", lname:"Ranieri"}, true];
console.log(stuff)
console.log(stuff[0])// first item is 0
console.log(stuff.length)

// for(let i = 0; i < stuff.length; i++){
//     console.log(stuff[i])
// }

// enhanced for loops also iterates through the array like line 10
// the variable element is the next item in the array
// useful if you DO NOT need the index
// for(const element of stuff){
//     console.log(element)
// }

// adding
stuff.push("Hello");// add an element to the end of the array
console.log(stuff);

// remove
stuff.pop()// removes the last element
console.log(stuff)

// arrays under the hood are really just objects with some methods attached
// if you wanted to you could edit and mutate any of the properties of an array
// I do NOT reccomend ever changing the in built properties of an array
stuff.length = 1000;
console.log(stuff.length);
stuff.push(100);
console.log(stuff.length);

const result = stuff.unshift("tim")
console.log(result)