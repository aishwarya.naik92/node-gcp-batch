let fun1 = function(){
    console.log("Hello")
}

let fun2 = fun1;

// every time you define a function you are creating a brand new function object in memory
fun1 = function(){
    console.log("Hola");
}

fun1() // hola
fun2() // hello

let obj1 = {fname:"Adam"} 
let obj2 = obj1;
obj1 = {fname:"steve"}// different objects
console.log(obj1.fname)
console.log(obj2.fname)