// The main thing that makes a programming language functional is that it supports
// First Class Functions (First Class Citizens)
// Functions ARE objects
// They can be created dynamically, they can be altered, they can be passed around just like any
// other variables in your code

const fname = "Adam";
const secondName = fname;
console.log(fname);
console.log(secondName);

// functions are objects
const hello = function(){
    console.log("Hello")
}
const secondHello = hello;

hello()
secondHello()
console.log(hello)// We are not invoking it we are printing out the function object

hello.something = "What's up";// functions are objects. Objects are always mutable
// so you could add properties to a function object

console.log(hello)