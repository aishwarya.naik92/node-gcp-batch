// A callback function is a function passed into another function as a parameter

const gutentag = function(){
    console.log("Gutentag")
}

const bonjour = function(){
    console.log("Bonjour")
}

// greet is a "Higher Order Function". A function that takes in functions
function greet(callback){
    callback()
}


function multiGreet(greetFunction, times){
    for(let i = 0; i< times; i++){
        greetFunction()
    }
}

// first parameter is a definition of some function
// second parameter is just a number for the loop
// multiGreet: Higher Order function
// gutentag: Callback function
multiGreet(gutentag,5);

