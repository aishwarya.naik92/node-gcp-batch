

const names = ["Adam", "Steve", "Bill"];
console.table(names)

const trainers = [
    {name:"Adam R", title:"Lead trainer"},
    {name:"Sierra N", title:"Senior trainer"},
    {name:"Richard O", title:"Tech Manager"}
]
console.table(trainers)

console.error("Error text")

console.time("Simple Add Operation") // labels must match
const x = 1 + 1;
console.timeEnd("Simple Add Operation")
