# Week 1 Review

## JavaScript
- Background
    - JS was designed to be a very flexible language above all else.
    - It was designed in 9 days so there are a lot of quirks.
    - Orginally designed for making web pages interactive but now used for everything.
- Features
    - Dyanamically Typed (Do not have to declare the type of variables when you create them)
    ```javascript
    let x = 100;
    let y = "Hello"
    ```
    - Loosely Typed (The datatypes of variables can be coerced implicitly)
    ```javascript
    const sum = true + true + true; // JS will cast the true values into 1's and add them
    ```
    - Interpreted Language
        - JS does NOT get compiled and then executed.
        - It is read one line at a time and executed immediately* after reading that line
    - High Level Language
        - Automatic memory management
        - Programmers cannot use pointers or make low level memory operations.
- Primitive Types
    - 6 primtive datatypes
    - Semantic term. These datatypes are STILL objects
    - null vs undefined
        - null: an explicit value a program must assign that means nothing
        - undefined: the default value of anything in JS
```javascript
    const name = "Adam" // string
    const age =  19 // number
    const isTrainer = true // boolean
    const nothing = null // null
    const undef = undefined // undefined
    // symbol
```
### Scopes in JS
- global
    - Available anywhere in the program
    - no key word
- function
    - Available anywhere in the function
    - var
- block
    - Available only within the block
    - let or const
```javascript

x = 1000; // global

function sayHello(){
    var greeting = "Hello"; // function
    console.log(greeing)
}

function add(num1,num2){
    const sum = num1 +num2;  // block
    return sum;
}
```
- Hoisting
    - All functions when executed go through a two pass system
    - JS wil redd through the function and assign a default of undefined to all var variables
        - BEFORE actually executing the code
    - You could use a var variable variable before it is actually defined.
    - You should avoid hoisting and let and const do not allow you to hoist anyway.

## Objects
- Objects are just a collection of key value pairs.
    - together a key value pair is known as property.
- Objects do not need to be created from a class. Classes did not exist for a LONG time in JS.
- Objects are always mutable.
    - You can always add, edit or remove properties from an object.
- **Object Literal** is the most common way to creat an object
```javascript
const dog = {name:"rover", owner:"Adam"} // object literal
// you can acces properites via 

// dot notation
dog.name

// bracket notation
dog["name"]
```
- Objects do have a __proto property that points to its Parent object and can inherit the parent object's properties.

- **Enhanced Object Literal**
    - Alternate syntax for object
```javascript
const dog = {
    name:"rover",
    bark(){
        conole.log("BARK!!!")
    }
}
```
## Truthy Falsy
- JS has very aggressie type coercion
- Every value in JS has inherit *truthiness*
- All values in JS are true except for the falsy values
    - The falsy values imply some kind of non-existence
```javascript
    // Falsy values
    const emptyString = '';
    const zero = 0;
    const nothing = null;
    const undef = undefined;
    const f = false;
    const notANumber = NaN
```
- == vs ===
    - == loose equality operator
        - Only checks if the values are the same and will do type coercion.
    - === strict equality operator
        - Checks both the type AND the value.
        - USE this 99% of the time.

## Arrays
- Arrays in JS are used to store many values.
- Arrays can store any data type.
- Arrays automatically resized themselves.
```javascript
const stuff = [10,20,"Hello", null, {fname:"Adam"}];

for(let i =0 ; i<stuff.length; i++){
    console.log(stuff[i]);
}

// Enhanced For loop
// for of loop
for(const item of stuff){
    console.log(item);
}
```
- Methods on the array object you should know
    - push(element)
    - pop()
    - filter(callback)
    - map(callback)

## Functions
- JS in my opinion is a mostly functional programming language.
    - Multi-paradigmed (cake-salad)
- Ways to create a function
```javascript

// option 1
function fun(){
}

// option 2
const fun = function(){
}

// Arrow notation
const fun = ()=>{
}

// if your function takes in  one param you can omit the parentheses
const print = e => {console.log(e)};

// if the function body is one line JS will implicty return the value and you do not need curly brackets
const add = (a,b) => a + b;
```
- Invoking functions
    - you invoke or execute functions using ()
    - JS does NOT support overloading.
        - The same function name but with different parameters
    - You can pass any arguments to a function JS will work with it
        - You can pass too arguments
        - You can pass too many arguments
        - You can pass arguments of the wrong type (no type checking)
- Default paramters
```javascript
    function greet(name = "Jane Doe"){
        console.log(name);
    }
    greet("Adam")// Adam
    greet()// Jane Doe
```
## Functional Programming
- Paradigm of progarmming
    - paradigm : style, way of thinking
    - Other paradigms
        - OOP
        - Delcarative
        - Imperative
- Places a heavy emphasis on functions
- The core tenet of any functional programming language is ***functions are objects***.
- Functions are first class citizens and can be stored in variables, passed around in functions, dynamically created just like any other value
- Core terminology
    - **Callback Function**
        - A function passed a *parameter* to another function
    - Higher Order function
        - A function that takes *in* a function as a parameter
    - Anonymous Function
        - A function defined 'on the spot' and was never stored in a variable or given a name.
    - Method
        - A function that is attached to an object
    - Constructor Functions
        - functions that create a new object
        - use the new keyword
        - capitalized by convention
    - Closure
        - A function that returns a function.
        - The returned function still has access to variables that were created in the outer function.
```javascript
function higherOrder(callback){
    callback()
}

const nums = [10,20,30,40];
nums.forEach(n=>{console.log(n)}) // anonymous callback function

const adam = {
    fname:"Adam",
    introduceSelf: function(){ // method
        console.log(this.fname)
    }
}

// constructor function
function Person(name, age){
    this.name = name;
    this.age = age;
}
const hank = new Person("Hank",30);

// closures
function outer(){
    let counter = 0; // counter gets "enclosed" in the inner function

    const inner = function(){
        counter++;
        console.log(counter);
    }

    return inner;
}
const in = outer();
in()// increases counter

const in2 = outer()
in2()// seperate counter variable 
``` 

## Asynchronous Programming
- JS is a single threaded event-driven programming.
- JS can only do 1 thing at a time.
- The Event Queue is where 'EVENTS' are stored. JS will process these events as they occur.
    - An express web server is a good example of an Event Driven program
    - JS is literally just listening waiting for an HTTP request
    - Then when an event DOES happen it responds with a function
- The problem with this system is that sometimes your code has to interact with files, or external resources. If it waited around for those external resources it would really slow down your program.
    - Analogy 
    - If you were told to set up for a party
        1. put out some silver ware
        2. clean up the living room
        3. order a pizza
            - Asynchronously handled
            - we do not literally stop moving and wait 30 minutes for it arrive
            - we move onto things that we can handle
        4. put out some drinks
        5. put the cats in the guest bedroom
- **Promises**
    - Some functions return promises which are "empty objects" 
        - pending
    - Promises will eventually have a value
        - Fulfilled 
        - Rejected (error is thrown)
```javascript
    const promise = readFile("data.txt")// returns a promise
    promise.then(callback).catch(errorHandlerCallbck)

    async function getData(){
        const data = await readFile("data.txt"); // data would be a fulfilled promise
    }
```
## ES6
- ECMAScript6
    - ECMA organization in charge of JS language specification
    - European Computer Manufacturers Association
        - Empty acronym now
- Features that ES6 added
    - Arrow Notation
    - async await
    - classes
    - enhanced object literals
    - string interpolation
    - promises
    - modules (import export syntax)

## Node JS
- Node JS is a runtime environment for JS
    - Node is more like the JVM than any sort of software framework
    - It just executes JS code
- JS used to be only be in browsers 
- Node can be used to create web servers, as well as many other types of programs
- Based on the chrome V8 engine.
- Every node project has a main configuration file called a ***package.json***
    - name
    - author
    - description
    - dependencies
    - scripts
- ***npm*** is node package manager
    - the main software tool for creating node projects
    - installing and managing dependencies
- npm repository
    - An online repository of open source JS code
- Important node commands
```bash
    # creating a node project
    npm init

    # intalling a dependency
    npm i dependency
    npm i express

    # runing a script defined in a package.json
    npm run scriptname
    npm run start
```
## Web Fundamentals
- The core way of communicating on the internet is ***HTTP***
- Hyper Text Transfer Protocol
    - Request Response based system
    - Every HTTP Request gives back HTTP Response
- **Client** person/browser/postman that *MAKES the HTTP Request*
- **Server** software/program/application that *REPLIES with an HTTP Response*
- HTTP Request
    - HTTP version
    - URL (where you are sending the request)
    - Headers (Key value pairs that contain information about the request)
    - Verb/Method (Type of Request)
        - GET 
            - Read
        - POST
            - Create
        - PUT
            - Update / replacement
        - PATCH
            - Update / modification
        - DELETE
            - Delete
    - Body
        - main content of a request.
- HTTP Response
    - HTTP Version
    - Headers
    - status code
        - 100's
            - Information
        - 200's
            - Success
        - 300's
            - redirect
        - 400's
            - client error
            - 404 not found
            - 403 forbidden
            - 415 not a teapot
            - 451 unavailable due to government censorship
        - 500's
            - server error
- **JSON**
    - JavaScript Object Notation
    - ***STRING*** version of a JavaScript Object Literal
    - The most popular format for sending information across web servers
    - Why JSON
        - JS is everywhere and all the programming languages wanted a fairly known and univeral standard.
        - It is just a string which is a data type that every programming language.
    - JSON syntax
        - All keys are in quotes
        - String values are in quotes
        - numbers are not in quotes
        - booleans are not in quotes
        - you can have nested object and arrat
```json
{
    "fname":"Adam", 
    "lname":"Ranieri",
    "age":19, 
    "isTrainer": true,
    "languages": ["Java", "Python", "JavaScript", "TypeScript"],
    "location": {"state":"West Virginia", "city": "Morgantown"}
}
```
## Express
- Express is a node dependency
- It is micro-framework for making web servers
- Unopinionated
    - Express does not force you to build application a certain way
    - The good thing about express is it doesn't tell you what to do
    - The bad thing about express is it doesn't tell you what to do
- Route Handling
```javascript
import express from 'express';

const app = express()

app.get("/path", (req, res)=>{
    res.send("Some data")
})

app.listen(3000, ()=>{console.log("Application started")})
```

## TypeScript
- Statically typed superset of JS
    - JavaScript that scales
- Any valid JS is valid TS
- Additional Features
    - Static Typing
    - Return Types
    - Classes before ES6
    - interfaces
    - Access Modifiers
- You cannot run a TS file.
    - Must be **transpiled** into JS first then run.
- Transpiling
    - tsc (TypeScript Compile)
    - tsconfig.json 
        - a configuration file for the TypeScipt transpiler
            - outDir
            - esmoduleinterop
            - target 
```TypeScript
const name:string = "Adam";

function isEven(num:number): boolean{
}
```