# REST
- **RE**presentational **S**tate **T**ransfer
- **Resource**
    - Some collection of things you are keeping track of
        - books, pets, people, computers, cars
- Uniform Interface
    - The standardized routes and methods to access and edit a resource
    - Resources are identified/located via the URI (Uniform resource identifier)    
        - /books, /pets, /computers
    - HTTP methods are used to indicate what the request is doing to the resource
        - GET 
            - Reading information
        - POST
            - Creating a new object in the resource
        - PUT 
            - Updating/replacement of a resource
        - PATCH 
            - Update/modification of a resource
        - DELETE
            - Removes a resource
- You return a representation of the resource in a universal format for the client
    - JSON
    - XML
    - pdf

# CI/CD/DevOps
- CI
    - Continuous Integration
    - Software Development practice where you continously add code to a remote repository
        - Often this code is tested to verify that it works correctly.
- CD
    - Continous Delivery
    - Software Development practice where you create deliverables or artifacts after pusing your code into a remote repository.
    - The next step after CI.
        - These artifacts/deliverables may end up being deployed
            - transpiled .js files would be an artifact of your TS application.
            - It is a product of your code. It is what COULD be run to make a server
- DevOps
- Portmanteau of **Dev**elopment and **Op**eration**s**
- Development
    - Testing and creating code
- Operations
    - Deploying, maintaining and making publicly available applications written in development
- Old school
    - Developers who just did the coding and testing
    - Operations/IT that deployed
- Modern School
    - Development and operations should be handled together.

# SQL 
- Relational Database
    - Store inforation in tables that **relate/connect** to each other
- Schema
    - The rules and structure of your database
- SQL (Structured Query Language)
    - The main programming language for databases
        - A bunch of different dialects with different syntaxes
- Sub-Langugages of SQL
- DDL (Data Definition Language)
    - Anything that defines the schema of the database
```sql
    create , alter, drop
```
- DML (Data Manipulation Language)
    - Anything that alters *data within* a table
```sql
insert, update set, delete
```
- DQL (Data Query Language)
    - Any statement that reads data
```sql
select
select * from coach where first_name = 'Bob'
```
- TCL (Transaction Control Language)
    - Resposnbile for creating and saving transactions
```sql
commit, rollback
```
- DCL (Data Control Language)
    - Responsible for assinging permissions for developers working on the database
```sql
grant, revoke
```
# Contraints
- Part of DDL
- They allow you to put restrictions on what can go in each column on a table.
```sql
primary key
not null
unique
check -- data has to meet a certain condition like greater than 0
foreign key
```

# Normalization
- Reducing redundancy in our database
- Not always a good thing.
    - pros
        - Increase Transaction speed
        - Decrease the amount of data stored
    - cons
        - Makes data more difficult to query
- 1NF
    - All records should be uniquely identifiable
        - Make sure you have a primary key
    - All data in the table should be atomic
        - Data cannot be broken down into smaller more meaningful units.
    - No array-like data in the table
        - A field is not a list of something
- 2NF
    - You are in 1NF
    - No Functional dependencies
        - One column cannot be calculated with the values in another column.
- 3NF
    - You are in 2NF
    - No Transitive dependencies
        - The data in one column cannot be found elswhere in the database.

# Multiplicities
- 1-1
    - one record matches to one record in another table.
    - player-stats
- 1-many
    - one record in a table matches to many records in another table.
    - player-team
- many-many
    - Many recoreds in a table match to many reocrds in another table.
    - player-game
    - Modeled using a junction table
        - Table with two foreign keys.

# Foregin Keys
- Constraint that you can put on a column
    - This column must refer to a unique column in another table
- The table with the foreign key is the **child**.
    - the Child is going to be the many in a multiplicty.
    - The table it refers to is the **parent**
- **Referential Integrity**
    - Foreign must ALWAYS point to a valid record
    - There are no orphan records
        - Records whose foregin keys point to records that do not exist.

# Joins and set operators
- Normalized databases spread data across multiple tables
    - This can make it difficult to read information
- Joins and set operators combine/denormalize tables to aid in making queries.
- Joins combine tables horizontally based on a join predicate 'on'
```sql
select * from player left join coach on player.t_id = coach.t_id;
left join -- all records in the left table plus matches on the right
right join -- all records in the right table plus matches on the left
inner join -- only records that match
cross join -- every record matched with every record
```
# Functions
- Scalar functions
    - Take in a sigle input and give a single output
```sql
Upper(x) 
lower(x)
length(x)
```
- Aggregate functions
    - Take in a group of values to give a single out put
    - can be paired with group by to create buckets of values
```sql
avg()
sum()
min()
count()

select avg(salary) from coach group by t_id;
```
# Transaction
- A discrete unit of one to many SQL commands
- RDMS strive to make their transactions **ACID** compliant
    - Atomic
        - Either all statments in a transaction will commit at the end or will be rolled back
    - Consistent
        - There is never a midway save point in a transaction.
    - Isolated
        - Concurrent transaction do not step on each other
            - Alter data in a way that may influence data in another transactions
            - Isolation levels are the amount of influence one transaction could have on another,
    - Durable
        - Transaction fail gracefully. There is no data corruption from an error.

# DAO
- Data Access Object
    - An object reponsible for performing CRUD operations in your application

# Full Stack Review
![Full Stack](fullstackanatomy.png)
- Application are built in layers
- Interface layer
    - Code that is resposinble for listening for HTTP requests and generating HTTP responses.
    - index.ts
    - Your inteface layer will call methods and functions from your service layer to properly handle these requests and generate responses.
- Service Layer
    - Code responsible for business rules
    - Requirements of the application that apply to real world circumstances.
        - When you checkout a book the return date should be two weeks.
        - You cannot checkout a book if it is the last available copy.
        - Check for invalid values
            - Can't create a book with no title.
    - services folder
    - This layers uses your persistence to make the changes a reality
- Persistence layer (Data Layer)
    - Code responsible for saving,updating and deleting information.
    - DOES NOT do logic for things like validating correct input values.
    - Very bare bones just does what is told
    - DAOS

# Tips for project 0
- Start with your entities
    - Make your tables
    - Entitiy fields to column names should be fairly 1-1
    - Avoid nested objects in your entitites
- Create your DAOS and get them tested!!!!!
- You really only need two tables
    - Account is a child of Client
- Take the routes one route at time
    - TEST with postman