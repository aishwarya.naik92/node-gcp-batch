
export default interface TemperatureCalculator{

    convertTemp(from:string, to:string, temp:number, precision?:number):number;

    isFreezing(from:string, temp:number):boolean;

    isMelting(from:string, temp:number):boolean;

}