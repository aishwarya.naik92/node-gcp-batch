import TempCalcImpl from "./temp-calc-impl";
const tempCalc = new TempCalcImpl();

test("Converts celcius to farenheit", ()=>{
    const result = tempCalc.convertTemp("C","F",250);
    expect(result).toBe(482)
})

test("Converts Farenheit to Celcius", ()=>{
    const result = tempCalc.convertTemp("F","C",-20);
    expect(result).toBe(-28.89);// two points of precision
})

test("Converts Farenheit to Celcius lowecase", ()=>{
    const result = tempCalc.convertTemp("f","c",-20);
    expect(result).toBe(-28.89);// two points of precision
})


test("A temperature below absolute zero", ()=>{

    try {
        tempCalc.convertTemp("F","C",-900);
    } catch (error) {
        expect(error.message).toBe("Input temperature of -900 is below absolute zero of -273 F")
    }

});