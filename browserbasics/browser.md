# Browser
- Software application that can do 3 things
    - HTML/CSS renderer
    - JS Engine
    - HTTP requests
- The primary way humans will interact with the internet and make HTTP requests
- Browsers and web pages are designed to be as human friendly as possible.
    - Theoretically you could browse the internet as purely text

# HTML
- Hyper Text Markup Language
- It is **NOT** a programming language
    - There is no way to do logic/conditionals
- HTML is used to define the structure and content of a web page
    - Headings, paragrphs, images etc...
- HTML is written using **element/tags**
    - Should always have a closing tag  
        - Self closing tags do exist
    - You can nest elements
    - Elements can have **attributes**
        - Qualifiers/adjectives that give additional properties to an element
            - id
            - name
            - class
            - src for images
```html
    <!--HTML Comment -->
    <h1>Hi I am a heading</h1>
    <p>I am a <b>paragraph</b></p>

    <img src="someimage.jpg"/>
```
# CSS
- Cascading Style Sheets
    - Add style to a web page
    - Aesthetics and colors etc...
- **Cascade Algorithim**
    - The order in which overalapping CSS selectors apply their styling
        - The golden rule
            - *The most specific CSS selector wins*
- CSS works by grabbing elements with **selectors**
    - These selectors then apply styling attributes called **properties**
- Type of selectors
    - Inline on the elment itself 
        - No explict selector 
    - Select by tag type
    - Select by ID
    - Select by CSS class
```css
    selector {key:value; key:value;}
    p {color:red; font-style:italic;}
```
- Getting elements to spread out correctly on a web page is a huge PAIN
    - float
    - center
    - auto
- Grid and Flexbox are explictly designed to help you with spacing elements

